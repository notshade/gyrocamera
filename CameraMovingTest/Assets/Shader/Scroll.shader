﻿Shader "Custom/Scroll"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
    }
    SubShader
    {
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf NoLight alpha:fade


        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        fixed4 _Color;

        void surf (Input IN, inout SurfaceOutput o)
        {
            // Albedo comes from a texture tinted by color
			fixed2 uv = IN.uv_MainTex;
			uv.y -= _Time.y;
            fixed4 c = tex2D (_MainTex, uv) * _Color;
            o.Albedo = c.rgb;
			o.Alpha = c.a;
            // Metallic and smoothness come from slider variables
        }

		float4 LightingNoLight(SurfaceOutput s, float3 lightDir, float atten)
		{
			float4 final;

			final.rgb = s.Albedo;
			final.a = s.Alpha;

			return final;

		}

        ENDCG
    }
    FallBack "Diffuse"
}
