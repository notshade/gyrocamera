﻿//#define USE_ACCELERATION
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraGyroMover : MonoBehaviour
{

	public Transform lookAt;
	public Transform camTransform;

	private Camera cam;

	[SerializeField]
	private float distance = 10.0f;
	
	private float currentX = 0.0f;
	private float currentY = 0.0f;

	//센서 반응도
	[SerializeField]
	private float sensivity = 4.0f;
	//원점 복구값 (tick)
	[SerializeField]
	private float restoring_force = 0.05f;


	[SerializeField]
	private float X_ANGLE = 15.0f;
	[SerializeField]
	private float Y_ANGLE = 15.0f;


	private float Y_ANGLE_MIN = 0;
	private float Y_ANGLE_MAX = 0;
	private float X_ANGLE_MIN = 0;
	private float X_ANGLE_MAX = 0;

	// Use this for initialization
	void Start()
	{
		camTransform = transform;
		cam = Camera.main;
	}

	List<Vector3> acceList = new List<Vector3>();
	List<Vector3> offsetList = new List<Vector3>();

	Vector3 Average (List<Vector3> forceList, Vector3 force)
	{
		float max = 10;
		forceList.Add(force);

		if (forceList.Count > max)
		{
			forceList.RemoveAt(0);
		}

		Vector3 result = Vector3.zero;
		float rate = (1 / max);
		for (int i= forceList.Count-1; i>=0; --i)
		{
			result += forceList[i] * 0.1f;
			result += forceList[i] * rate;
		}
		return result;
	}

	Vector2 vtRevertMove = Vector2.zero;
	// Update is called once per frame
	Vector3 nowMove = Vector3.zero;

#if !USE_ACCELERATION
	Vector3 Mouse = Vector3.zero;

#endif
	void Update()
	{
		Y_ANGLE_MIN = -Y_ANGLE;
		Y_ANGLE_MAX = Y_ANGLE;
		X_ANGLE_MIN = -X_ANGLE;
		X_ANGLE_MAX = X_ANGLE;


		{
			//Vector3 nowMove = Input.acceleration;

#if USE_ACCELERATION
			nowMove = Average(acceList, Input.acceleration);
#else
			Vector3 moveXY = Vector3.zero;
			moveXY.y = Input.GetAxis("Mouse Y");
			moveXY.x = -Input.GetAxis("Mouse X");

			Mouse += (moveXY/ 10);
			nowMove = Average(acceList, Mouse);

#endif

			currentX = nowMove.y * sensivity;
			currentY = nowMove.x * sensivity;

			//원점으로 돌아가려는 성질
			float X_ANGLE_CENTER = X_ANGLE_MAX + (X_ANGLE_MIN < 0 ? X_ANGLE_MIN : -X_ANGLE_MIN);
			float Y_ANGLE_CENTER = Y_ANGLE_MAX + (Y_ANGLE_MIN < 0 ? Y_ANGLE_MIN : -Y_ANGLE_MIN);
			vtRevertMove.x = Mathf.Lerp(vtRevertMove.x, currentX, restoring_force);
			vtRevertMove.y = Mathf.Lerp(vtRevertMove.y, currentY, restoring_force);

			currentX -= vtRevertMove.x;
			currentY -= vtRevertMove.y;

			currentX = Mathf.Clamp(currentX, X_ANGLE_MIN, X_ANGLE_MAX);
			currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
		}
	}

	public float GetRateY()
	{
		float Y_ANGLE_LEN = (Y_ANGLE_MAX - Y_ANGLE_MIN);

		float rate = Mathf.Abs(currentY - Y_ANGLE_MIN) / Y_ANGLE_LEN;
		//Debug.Log($"{rate} = ({currentY} - {Y_ANGLE_MIN}) / {Y_ANGLE_LEN} ");
		return rate;
	}

	private void LateUpdate()
	{
		Vector3 dir = new Vector3(0, 0, -distance);
		Quaternion rotation = Quaternion.Euler(currentX, currentY, 0);
		camTransform.position = lookAt.position + rotation * dir;
		//camTransform.LookAt(lookAt.position);

		Debug.DrawLine(Vector3.zero, rotation * dir);

		Vector3 leftTop = Quaternion.Euler(X_ANGLE_MIN, Y_ANGLE_MIN, 0) * dir;
		Vector3 rightBottom = Quaternion.Euler(X_ANGLE_MAX, Y_ANGLE_MAX, 0) * dir;

		Debug.DrawLine(leftTop, new Vector3(rightBottom.x, leftTop.y, leftTop.z));
		Debug.DrawLine(rightBottom, new Vector3(rightBottom.x, leftTop.y, leftTop.z));
		Debug.DrawLine(leftTop, new Vector3(leftTop.x, rightBottom.y, leftTop.z));
		Debug.DrawLine(rightBottom, new Vector3(leftTop.x, rightBottom.y, leftTop.z));

		//Debug.DrawLine(Vector3.zero, Quaternion.Euler(X_ANGLE_MAX, 0, 0) * dir);
		//Debug.DrawLine(Vector3.zero, Quaternion.Euler(0,0,Y_ANGLE_MIN) * dir);
		//Debug.DrawLine(Vector3.zero, Quaternion.Euler(0, 0, Y_ANGLE_MAX) * dir);
	}

	

}
