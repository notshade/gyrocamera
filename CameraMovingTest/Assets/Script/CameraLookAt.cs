﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLookAt : MonoBehaviour
{
	[SerializeField]
	private MeshRenderer[] mesh;
	private List<Material> materialList = new List<Material>();
	private CameraGyroMover camerMover;

	[SerializeField]
	private bool blookAtCamera = false;

	[SerializeField]
	private string colorProperty = "_Color";
	[SerializeField]
	private bool bColorProperty = true;

	private void Start()
	{
		camerMover = Camera.main.GetComponent<CameraGyroMover>();
		for (int i=0; i< mesh.Length; ++i)
		{
			materialList.Add(mesh[i].materials[0]);
		}
	}


	// Update is called once per frame
	void Update()
    {
		SetMaterial(camerMover.GetRateY());
    }

	private void LateUpdate()
	{
		if (blookAtCamera)
		{
			transform.LookAt(Camera.main.transform);
		}
	}

	void SetMaterial(float fValue)
	{
		if (materialList.Count == 0)
			return;

		float j = 0.5f;
		float a = -6 * j;
		float k = 2.5f * j;

		float max = materialList.Count;
		fValue *= max;

		for (int i = 0; i < materialList.Count; ++i)
		{
			//Color color = materialList[i].color;

			float h = 0.5f + i;
			float alpha = Mathf.Clamp(a * Mathf.Pow((fValue - h), 2) + k, 0, 1);

			//양쪽끝값은 무조건 1
			if (i == 0)
			{
				alpha = (fValue < 0.5f) ? 1 : alpha;
			}
			else if (i == materialList.Count - 1)
			{
				alpha = (fValue > max - 0.5f) ? 1 : alpha;
			}

			if (bColorProperty)
			{
				SetShaderAlphaByColor(materialList[i], alpha);
			}
			else
			{
				SetShaderAlpha(materialList[i], alpha);
			}
		}
	}

	private void SetShaderAlpha(Material material, float alpha)
	{
		material.SetFloat(colorProperty, alpha);
	}

	private void SetShaderAlphaByColor (Material material, float alpha)
	{
		Color color = material.GetColor(colorProperty);
		color.a = alpha;
		material.SetColor(colorProperty, color);
	}
}
